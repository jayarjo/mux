Ext.define('Mux.form.field.MultiSelect', {
    extend: 'Ext.ux.form.MultiSelect',
    
    alias: 'widget.xmultiselect',
    
    delimiter: null,
    
    preload: true,
    
    initComponent: function() {
        if ((this.storeUrl || this.proxy) && !this.store) {  
            var storeConfig = {
                // this store shouldn't be reused
                storeId: 'store-' + (new Date),
                autoLoad: false,      
                  
                // proxies may vary
                proxy: this.proxy || {
                    type: 'ajax',
                    url: this.storeUrl,
                    pageParam: false,
                    startParam: false,
                    limitParam: false,
                    reader: {
                        rootProperty: 'topics',
                        totalProperty: 'totalCount'
                    },
                    writer: {
                        nameProperty: 'mapping'
                    }
                }
            };
            
            if (this.model) {
                storeConfig.model = this.model;
            } else {
                // should be enough to feed the combo with values
                storeConfig.fields = [
                    { name: this.valueField },
                    { name: this.displayField }
                ];
            }
            this.store = Ext.create('Ext.data.Store', storeConfig);
        } 
        
        this.callParent(arguments);
        
        if (this.store && this.store.on) {
            this.store.on('load', function() {
                this.isLoaded = true;
                
                if (this.defaultValue) {
                    this.suspendEvents(false);
                    this.setValue(this.defaultValue);
                    this.resumeEvents();
                }
            }, this, { single: true });
        }
        
        if (this.defaultValue && this.preload && this.store) {
            this.store.load();
        }
    }
});

