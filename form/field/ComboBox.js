Ext.define('Mux.form.field.ComboBox', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.xcombobox', // combobox on steroids
    
    requires: [
        'Mux.data.Model',
        'Ext.data.identifier.Generator'
    ],
    
    preload: false,
    cloneStore: false,
    isLoaded: false,
    
    initComponent: function() {
        
        if ((this.storeUrl || this.proxy) && !this.store) {  
            var storeConfig = {
                // this store shouldn't be reused
                storeId: 'store-' + (new Date),
                autoLoad: false,      
                  
                // proxies may vary
                proxy: this.proxy || {
                    type: 'ajax',
                    url: this.storeUrl,
                    pageParam: false,
                    startParam: false,
                    limitParam: false,
                    reader: {
                        rootProperty: 'data',
                        totalProperty: 'total'
                    },
                    writer: {
                        nameProperty: 'mapping'
                    }
                }
            };
            
            if (this.model) {
                storeConfig.model = this.model;
            } else {
                // should be enough to feed the combo with values
                storeConfig.fields = [
                    { name: this.valueField },
                    { name: this.displayField }
                ];
            }
            this.store = Ext.create('Ext.data.Store', storeConfig);
        } 
        // make sure that we always have unique standalone store if typeAhead is enabled
        else if (this.store && this.typeAhead && this.cloneStore) {
            var storeConfig = {
                storeId: 'store-' + (new Date)
            };
            var origStore = Ext.data.StoreManager.lookup(this.store);
            if (origStore) {
                this.store = Ext.create(origStore.self.getName(), storeConfig);
            } else {
                this.store = Ext.create(this.store, storeConfig);
            }
        }
        
        this.callParent(arguments);       
        
        if (this.store && this.defaultValue) {
            if (this.defaultDisplayValue) {
                var data = {};
                data[this.valueField] = this.defaultValue;
                data[this.displayField] = this.defaultDisplayValue;
          
                this.store.loadData([data]);
                
                this.suspendEvents(false);
                this.setValue(this.defaultValue);
                this.resumeEvents();
            }
            
            if (!this.isLoaded) {
                this.store.on('load', function() {
                    this.isLoaded = true;

                    if (this.defaultValue && !this.defaultDisplayValue) {
                        this.suspendEvents(false);
                        this.setValue(this.defaultValue);
                        this.resumeEvents();
                    }
                }, this, { single: true });
                
                if (this.preload) {
                    this.store.load();
                }
            }
        }
    },          
    
    // preload the store        
    beforeQuery: function(queryPlan) {        
        if (!this.isLoaded && this.store && !this.store.isLoading() && this.queryMode === 'local') {
            this.doRemoteQuery({
                query: '',
                rawQuery: true,
                forceAll: false,
                combo: this,
                cancel: false
            });
            return false;
        } else {
            return this.callParent(arguments);
        }
    },
            
    // make local search to match not only beginning of the string, but parts in the middle as well     
    doQuery: function(queryString, forceAll, rawQuery){
        var me = this;

        
        if(me.queryMode === 'local' && queryString && queryString.length >= me.minChars){
            queryString = Ext.String.escapeRegex(queryString);
            if (me.anyMatch !== true) {
                queryString = '^' + queryString;
                if (me.exactMatch === true) {
                    queryString += '$';
                }
            }
            queryString = new RegExp(queryString, me.caseSensitive ? '' : 'i');
            queryString.length = me.minChars; // this is a trick
        }
        
        me.callParent([queryString, forceAll, rawQuery]);
    },
            
            
    onTriggerClick: function() {
        // clear all filters on expand
        if (this.queryMode === 'local') {
            this.store.clearFilter();
        }
        this.callParent(arguments);
    },
            
    
    // when item is picked up, properly set value + display (for some reason by default it's just display)
    onTypeAhead: function() {
        var me = this,
            displayField = me.displayField,
            valueField = me.valueField || displayField,
            record = me.store.findRecord(displayField, me.getRawValue()),
            boundList = me.getPicker(),
            newValue, newDisplayValue, len, selStart;

        if (record) {
            newValue = record.get(valueField);
            newDisplayValue = record.get(displayField);
            len = newDisplayValue.length;
            selStart = me.getRawValue().length;

            boundList.highlightItem(boundList.getNode(record));

            if (selStart !== 0 && selStart !== len) {
                me.setValue(newValue);
                me.selectText(selStart, newDisplayValue.length);
            }
        }
    },

    
    // override native private assert method (very bad) to allow empty value under forceSelection: true
    // one can still forbid an empty value by setting allowBlank to false 
    assertValue: function() {
        var me = this,
            value = me.getRawValue(),
            rec, currentValue;

        if (me.forceSelection) {
            if (me.multiSelect) {
                // For multiselect, check that the current displayed value matches the current
                // selection, if it does not then revert to the most recent selection.
                if (value !== me.getDisplayValue()) {
                    me.setValue(me.lastSelection);
                }
            } else {
                // For single-select, match the displayed value to a record and select it,
                // if it does not match a record then revert to the most recent selection.
                rec = me.findRecordByDisplay(value);
                if (rec) {
                    currentValue = me.value;
                    // Prevent an issue where we have duplicate display values with
                    // different underlying values.
                    if (!me.findRecordByValue(currentValue)) {
                        me.select(rec, true);
                    }
                } else if (rec === "") {                        // <--
                    me.setValue("");                            // <--
                } else {
                    me.setValue(me.lastSelection);
                }
            }
        }
        me.collapse();
    },

    // if value is empty, return empty instead of false (default behavior)
    // works in conjunction with assertValue above
    findRecord: function(field, value) {
        if (this.forceSelection && !Ext.String.trim(value)) {   // <--
            return '';                                          // <--
        }
        return this.callParent(arguments);
    }
        
    
});
