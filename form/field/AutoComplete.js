Ext.define('Mux.form.field.AutoComplete', {
    extend: 'Mux.form.field.Dependobox',
    
    xtype: 'autocomplete',
    
    requires: [
        'Mux.data.FilterStore'
    ],
    
    autoSelect: true,
    forceSelection: true,
    hideTrigger: true,
    typeAhead: true,
    editable: true,
    matchFieldWidth: false,
    queryMode: 'remote',
    
    listConfig: {
        loadingText: 'ძიება...',
        emptyText: 'ვერაფერი მოიძებნა'
    },
    
    listeners: {
        change: function() {
            var me = this;
            var params = {};
            Ext.each(me.filters, function(itemId) {
                var comp = me.up('form').down('#'+itemId);
                var value = comp.getValue();
                if (value) {
                    params[comp.storeMapping || itemId] = value;
                    return false;
                }
            });
            me.store.getProxy().extraParams = params;
        }
    },
            
    
    initComponent: function() {
        var me = this;

        this.listConfig = {
            listeners: {
                itemclick: function(list, record) {
                    var form = this.up('form');
                    var stores = {};
                    
                    function loadRecord() {
                        Ext.each(me.filters, function(itemId) {
                            var comp = form.down('#'+itemId);
                            comp.setValue(record.get(comp.storeMapping || itemId));
                        });
                    }

                    Ext.each(me.filters, function(itemId) {
                        var item = form.down('#'+itemId);

                        if (!item.store || item.dependsOn && !record) {
                            return true; // continue
                        }

                        var storeId = typeof item.store === 'string' ? item.store : item.store.storeId;
                        if (!storeId) {
                            return true; 
                        }

                        stores[storeId] = {};

                        // surpress onchange events;
                        if (record) {
                            item.suspendCheckChange++;
                        }

                        if (item.dependsOn) {
                            var depComp = form.down('#'+item.dependsOn);
                            if (!depComp || Ext.Array.indexOf(['combobox', 'dependobox'], depComp.xtype) === -1) {
                                return true;
                            } 
                            
                            var itemId = depComp.storeMapping || depComp.itemId;
                            stores[storeId][itemId] = record.get(itemId);
                        }
                    });

                    if (!Mux.util.Util.isEmpty(stores)) {
                        var storeReloader = Ext.create('Mux.util.StoreReloader');

                        storeReloader.on('load', function() {
                            if (record) {
                                loadRecord();
                                Ext.each(me.filters, function(itemId) {
                                    var item = form.down('#'+itemId);
                                    if (item.store) {
                                        item.suspendCheckChange--;
                                        item.enable();
                                    }
                                });
                            }
                            form.setLoading(false);
                        });

                        form.setLoading(true);
                        storeReloader.load(stores);
                    } else {
                       if (record) {
                            loadRecord();
                        }
                    }
                }
            }
        };

        this.callParent(arguments);
    }
    
});
