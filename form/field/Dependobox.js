Ext.define('Mux.form.field.Dependobox', {
    extend: 'Mux.form.field.ComboBox',
    
    alternateClassName: 'Mux.form.Dependobox',
    alias: ['widget.dependobox', 'widget.dependo'],

    
    anyMatch: true,
    caseSensitive: false,
    queryMode: 'local',
    forceSelection: true,
    typeAhead: true,
    editable: true,
    
    disableDependentField: true,    
    blockSyncWith: false,
  
            
    // we do this after component is rendered to be sure that dependencies are ready
    afterRender: function() {
        var me = this;
        
        if (this.dependsOn) {
            this.attachDependencies();
        }
        
        if (this.syncWith) {
            this.on('change', function(field, value, oldValue) {
                if (!me.blockSyncWith) {
                    me.syncWithField();
                }
            });
        }
        
        this.callParent(arguments);
    },
            
    
    attachDependencies: function(deps) {
        var me = this;
        
        deps = deps || this.dependsOn;
        if (!deps) {
            return;
        }
                
        function reloadStore(deps, index) {
            if (!me.store) {
                return false;
            }
            
            var depId = deps[index];
            if (!depId) {
                return false;
            }
            
            var depComp = me.up('form').down('[itemId='+depId+']');
            var value = depComp.getValue();
            if (!value) {
                if (deps[index - 1]) {
                    return reloadStore(deps, index - 1); // try dependency with higher priority, if any
                } else if (this.disableDependentField) { // if field is not meant to get disabled for empty dependencies, it should auto restore it's initial set of values
                    return false;
                }                
            }
            
            var params = {};
            params[depComp.storeMapping || me.store.model && me.store.model.getMapping && me.store.model.getMapping(depComp.itemId) || depComp.name] = value;
            me.store.load({ params: params });
            me.lastQuery = ""; // this makes sure that combo will not reload if it's already loaded and queryMode is 'remote'
            return true;
        }
        
        
        if (deps) {
            if (!Ext.isArray(deps)) {
                deps = [deps];
            }
            
            Ext.each(deps, function(depId, index) {
                var depComp = me.up('form').down('[itemId='+depId+']');
                
                depComp.on('change', function(depComp, value, oldValue) {
                    if (value && !Ext.isNumeric(value)) {
                        return false;
                    }
                    
                    // clear the listbox and the value
                    if (me.store) {
                        me.store.loadData([], false);
                        me.store.lastQuery = null;
                    }
                    me.reset();
                    
                    if (reloadStore(deps, index)) {
                        me.enable();
                    } else if (me.disableDependentField) {
                        me.disable();
                    }
                });
            });
        }
    },
    

    syncWithField: function(syncWith) {
        syncWith = syncWith || this.syncWith;
        if (!syncWith) {
            return;
        }

        var field = Ext.ComponentQuery.query(syncWith);
        if (this.isValid() && field && field.length) {
            field = field[0];
            var value = this.getValue();
            
            field.blockSyncWith = true;
            if (!field.store || field.isLoaded) {
                field.setValue(value);
                field.blockSyncWith = false;
            } else {
                field.store.load({
                    callback: function() {
                        field.setValue(value);
                        field.blockSyncWith = false;
                    } 
                });
            }

            if (field.disabled) {
                field.enable();
            }
        }
    }
});

