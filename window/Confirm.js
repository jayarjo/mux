Ext.define('Mux.window.Confirm', {
    extend: 'Ext.window.Window',
    
    alias: 'widget.windowconfirm',
    
    title: 'გაფრთხილება',
    msg: "ნამდვილად გსურთ მოცემული ჩანაწერის წაშლა?",
            
    height: 160,
    width: 350,
    modal: true,
    bodyPadding: 10,
    bodyStyle: 'border: none; background: transparent',
    contentHeight: 50,
    autoShow: true,
    buttonAlign: 'center',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    
    timestamp: false,
        
    initComponent: function() {
        var me = this;
        
        this.items = [
            {
                xtype: 'container',
                height: this.contentHeight,
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        xtype: 'container',
                        html: Ext.String.format('<div class="x-component x-message-box-icon x-box-item x-component-default x-dlg-icon {0}"></div>', this.icon),
                        height: 50,
                        width: 40,
                        margin: '0 10 0 0'
                    },
                    {
                        xtype: 'container',
                        html: this.msg,
                        height: this.contentHeight
                    }
                ]
            }
        ];
        
        if (this.timestamp) {
            this.items.push({
                xtype: 'datefield',
                itemId: 'timestamp',
                fieldLabel: "თარიღი",
                name: 'end_date',
                labelWidth: 60,
                format: 'd F, Y',
                submitFormat: 'Y-m-d H:i:s',
                value: new Date()
            });
        }
        
        this.icon = null; // reset
        
        if (!this.buttons) {
            this.buttons = [
                {
                    xtype: 'button',
                    text: "კი",
                    itemId: 'yes',
                    handler: function(btn) {
                        var timestamp;
                        if (me.timestamp) {
                            timestamp = btn.up('window').down('#timestamp').getSubmitValue();
                        }

                        if (Ext.isFunction(me.fn)) {
                            Ext.callback(me.fn, me, [btn.itemId, timestamp]);
                        }
                    }
                },
                {
                    xtype: 'button',
                    text: "არა",
                    itemId: 'no',
                    handler: function(btn) {
                        btn.up('window').close();

                        if (Ext.isFunction(me.fn)) {
                            Ext.callback(me.fn, me, [btn.itemId]);
                        }
                    }
                }
            ];
        }
        
        this.callParent(arguments);
    }
});
