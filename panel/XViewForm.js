Ext.define('Mux.panel.XViewForm',  {
    extend: 'Ext.form.Panel',
    
    alias: 'widget.xviewform',
    
    requires: [
        'Mux.util.Util'
    ],
    
    title: "ჩანაწერის რედაქტირება",
    
    record: null,
    schema: null,
    
    reference: 'xviewform',
    frame: false,
    bodyPadding: 10,
    width: 650,
    autoScroll: true,
    
    defaults: {
        anchor: '100%',
        xtype: 'textfield',
        labelWidth: 200
    },
    
    items: [],
    
    dockedItems: [
        {
            xtype: 'toolbar',
            itemId: 'action-toolbar',
            dock: 'bottom',
            layout: {
                pack: 'end',
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'გაუქმება',
                    itemId: 'cancel',
                    iconCls: 'icon-cancel'
                },
                {
                    xtype: 'button',
                    text: 'შენახვა',
                    formBind: true,
                    itemId: 'save',
                    iconCls: 'icon-save'
                }
            ]
        }
    ],
    
    initComponent: function() {        
        if ((!this.items || !this.items.length) && this.schema) {
            this.items = this._generateFields(this.schema, this.record);
        }
            
        
        this.callParent(arguments);
    },


    _generateFields: function(schema, record) {
        var me = this;
        var fields = [];
        
        Ext.each(schema, function(item) {
            var field = {
                fieldLabel: item.tip || item.label,
                name: item.name,
                itemId: item.itemId || item.name.replace(/\W/g, '_')
            };
            
            if (record) {
                field.value = record.get(item.name) || item.value;
            }

            if (item.fldExclude) {
                return true;
            }

            switch (item.type) {
                case 'id': 
                    Ext.apply(field, {
                        xtype: 'hiddenfield',
                        name: 'id'
                    });
                    break;

                case 'combo':
                    field.xtype = 'dependobox';
                    
                    if (item.fldCfg && item.fldCfg.storeUrl) {
                        if (!item.valueName) {
                            var m = item.name.match(/^(.+)_name$/);
                            if (m) {
                                item.valueName = m[1] + '_id';
                            }
                        }
                        
                        Ext.apply(field, {
                            valueField: 'id',
                            displayField: item.name,
                            name: item.valueName
                        });
                        
                        if (record) {
                            field.defaultValue = record.get(item.valueName);
                            field.defaultDisplayValue = record.get(item.name);
                        }
                    }
                    break;

                case 'int':
                    field.xtype = 'numberfield';
                    break;

                case 'date':
                    Ext.apply(field, {
                        xtype: 'datefield',
                        format: item.outFormat || 'd F, Y',
                        submitFormat: 'Y-m-d H:i:s',
                        altFormats: item.inFormat
                        
                    });
                    break;
                    
                case 'bool':
                    Ext.apply(field, {
                        xtype: 'checkbox', 
                        inputValue: 1,
                        uncheckedValue: 0
                    });
                    
                    if (record) {
                        field.checked = record.get(item.name);
                    }
                    break;
                  
                case 'hidden':
                    field.xtype = 'hiddenfield';
                    break;
                    
                case 'text':
                    Ext.apply(field, {
                        xtype: 'textareafield',
                        grow: false,
                        height: 50
                    });
                    break;
                    
                case 'html':
                case 'richtext':
                    Ext.apply(field, {
                        xtype: 'htmleditor',
                        height: 90
                    });
                    break;
                    
                case 'string':
                    field.xtype = 'textfield';
                    break;

                default:
                    if (Ext.isFunction(me[item.type])) {
                        field = Ext.callback(item.type, me, [item]);
                    } else {
                        return true;
                    }
            }

            if (Ext.isObject(item.fldCfg)) {
                Ext.Object.merge(field, item.fldCfg);
            }
            
            if (item.required) {
                field.allowBlank = false;
                field.afterLabelTextTpl= Mux.util.Util.required;
            }

            fields.push(field);
        });            
    }
});
