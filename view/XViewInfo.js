Ext.define('Mux.view.XViewInfo', {
    extend: 'Ext.view.View',
   
    alias: 'widget.info',
    
    title: 'ძირითადი ინფორმაცია',
            
    layout: 'fit',
    border: false,
    autoScroll: true,
    
    minHeight: 300,
    
    itemTpl: '{info}',
    
    store: null,
    
    listeners: {
        afterrender: {
            fn: '_monExpander',
            scope: 'this'
        }
    },
    
    initComponent: function() {
        
        if (!this.store && this.storeParams) {
            this.store = Ext.create('Ext.data.Store', Ext.Object.merge({    
                fields: ['info'],
                autoLoad: true,
                proxy: {
                    type: 'ajax'
                }
            }, this.storeParams));
        }
        
        this.callParent(arguments);
    },
            
            
    _monExpander: function() {
        this.mon(this.getEl(), 'click', function(event, target, eOpts) {
            var target = Ext.get(target);                        
            if (target.hasCls('expander')) {
                Ext.get(target.findParent('.detail_tbl')).toggleCls('collapsed');
            }
        });
    }
});
