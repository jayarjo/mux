Ext.define('Mux.XView', {
    extend: 'Ext.grid.Panel',
    
    requires: [
        'Mux.XViewController',
        'Mux.panel.XViewForm',
        'Mux.view.XViewInfo',
        'Mux.data.Model',
        'Mux.data.DataStore',
        'Mux.toolbar.FilterBar',
        'Mux.form.field.Dependobox'
    ],
    
    controller: 'xview',
    
    itemId: 'datagrid',
    forceFit: false,
    stateful: true,
    itemCls: 'datagrid',
    
    store: null,
    
    windowCfg: null,
    
    deleteTimestamp: false, // output date field in delete confirmation window
    
    layout: {
        type: 'fit'
    },
    
    actionColumns: true,
    exportToExcel: false,
    reopenAfterInsert: true,
    
    constructor: function(config) {
        // class extension in extjs doesn't seem to be recursive, hence this workaround
        config.windowCfg = Ext.Object.merge({
            showInfo: false,
            editable: true,
            xbody: 'Mux.panel.XViewForm'
        }, this.windowCfg, config.windowCfg);
        
        this.callParent([config]);
    },
            
        
    initComponent: function() {
        var me = this;
        
        var classParts = this.self.getName().split('.').slice(-2); // e.g. Soft.view.iptvbilling.Currency
        var xtype = classParts.join('').toLowerCase();  
        
        this.stateId = xtype + 'grid';
        
        if (!this.permToManage) {
            this.permToManage = classParts.join('') + 'Manage';
        }
        
        this.actionColumns = Soft.User.can(this.permToManage);
        this.disableDoubleClickEdit = !this.actionColumns;
        
        if (!this.store) {
            this.store = this._generateStore();
        }
        
        if (!this.columns) {
            this.columns = this._generateColumns();
        }
        
        this.columns = {
            items: this.columns,
            defaults: {
                getStateId: function() {
                    return [me.stateId, this.dataIndex || this.menuText || this.text].join('_');
                }
            }
        };
        
        if (!this.tbar) {
            this.tbar = Ext.create('Mux.toolbar.FilterBar', {
                items: [
                    {
                        xtype: 'button',
                        tooltip: 'ახალი ჩანაწერის დამატება',
                        iconCls: 'icon-add',
                        itemId: 'add'
                    }
                ]
            });
        }
            
        if (!this.bbar) {
            this.bbar = Ext.create('Ext.PagingToolbar', {
                store: this.store,
                displayInfo: true,
                displayMsg: 'ჩანაწერები {0} - {1} of {2}',
                emptyMsg: "ჩანაწერები ვერ მოიძებნა",
                items: !this.exportToExcel ? [] : [
                    '->',
                    {
                        xtype: 'button',
                        itemId: 'download-excel',
                        iconCls: 'icon-excel',
                        text: 'ჩამოტვირთვა'
                    }
                ] 
            });
        }
                
        this.callParent(arguments);        
    },
            
            
    _generateColumns: function() {
        var columns = [];
        Ext.each(this.schema, function(item) {
            if (item.colExclude) {
                return true;
            }

            var column = {
                text: item.label,
                dataIndex: item.name,
                sortable: item.hasOwnProperty('sortable') ? item.sortable : true,
                tooltip: item.tip || item.tooltip
            };

            switch (item.type) {
                case 'date':
                    column.xtype = 'datecolumn';
                    column.format = item.outFormat;
                    break;

                case 'bool':
                    column.xtype = 'booleancolumn';
                    break;
            }

            if (item.colCfg && Ext.isObject(item.colCfg)) {
                Ext.apply(column, item.colCfg);
            }

            if (!column.width && !column.flex) {
                column.flex = 1;
            }

            columns.push(column);
        });

        if (this.actionColumns) {
            columns.push(
                {
                    xtype: 'actioncolumn',
                    width: 30,
                    align: 'center',
                    sortable: false,
                    resizable: false,
                    menuText: 'რედაქტირება',
                    menuDisabled: true,
                    items: [
                        {
                            handler: function(view, rowIndex, colIndex, item, e) {
                                var store = view.up('grid').getStore();
                                this.fireEvent('itemedit', view, store ? store.getAt(rowIndex) : null);
                            },
                            iconCls: 'icon-grid_edit',
                            tooltip: 'რედაქტირება'
                        }
                    ]
                },
                {
                    xtype: 'actioncolumn',
                    width: 30,
                    align: 'center',
                    sortable: false,
                    resizable: false,
                    menuText: 'წაშლა',
                    menuDisabled: true,
                    items: [
                        {
                            handler: function(view, rowIndex, colIndex, item, e) {
                                var store = view.up('grid').getStore();
                                this.fireEvent('itemdelete', view, store ? store.getAt(rowIndex) : null);
                            },
                            iconCls: 'icon-delete',
                            tooltip: 'წაშლა'
                        }
                    ]
                }
            );
        }
        return columns;
    },
      
            
    _generateStore: function() {
        // fields for the model
        var items = [];
        Ext.each(this.schema, function(item) {
            var item = {
                name: item.name,
                type: item.type || 'string'
            };
            
            if (item.type === 'date' && item.inFormat) {
                item.dateFormat = item.inFormat;
            }
            items.push(item);
        });
        
        var model = Ext.create('Mux.data.Model', {
            fields: items
        });
        
        // urlPattern for the store
        if (!this.urlPattern) {
            var classParts = this.self.getName().split('.').slice(-2);
            this.urlPattern = Ext.String.format('load.php?a={0}.{1}.', classParts[0], classParts[1]) + '{0}';
        }
        
        // create the store
        return Ext.create('Mux.data.DataStore', Ext.Object.merge({
            model: model,
            autoLoad: false,
            urlPattern: this.urlPattern
        }, this.storeParams || {}));
    },
            
            
    _generateWindowBody: function(record) {
        var me = this;
        
        if (!this.windowCfg) {
            return null;
        }
        
        var body = Ext.create(this.windowCfg.xbody || this.xbody, {
            autoScroll: true,
            minHeight: 200,
            schema: this.schema,
            xview: this,
            record: record
        });
        
        // enable info tab if requested
        if (this.windowCfg.showInfo && record) {            
            var infoTab = Ext.create('Mux.view.XViewInfo', {
                storeParams: {
                    proxy: {
                        url: this.store.getProxy().api.info,
                        extraParams: {
                            id: record.get('id')
                        }
                    }
                }
            });
            
            if (!body.is('tabpanel')) {
                var tab = body;
                body = Ext.create('Ext.tab.Panel', {
                    schema: this.schema,
                    xview: this,
                    record: record
                });
                body.add(tab);
            }
            
            body.insert(0, infoTab);
            body.setActiveTab(infoTab);
        }
        
        
        if (!this.windowCfg.editable) {
            if (body.is('tabpanel')) {
                var formTab = body.down('xviewform');
                if (formTab) {
                    body.remove(formTab);
                }
            }
        } else {
            // essential listeners
            body.down('#save').on({
                click: '_onEditSubmitClick',
                scope: this.controller
            });

            body.down('#cancel').on({
                click: '_onEditCancelClick',
                scope: this.controller
            });
        }
        
        
        if (body.is('tabpanel')) {
            body.on({
                tabchange: '_onTabChange',
                scope: this.controller
            });
        }
        
        
        // if other window related listeners are declared, attach them
        if (this.controller && this.controller._control) {
            Ext.iterate(this.controller._control, function(selector, listeners) {
                if (/^window\s/.test(selector)) {
                    Ext.iterate(listeners, function(eType, fn) {
                        var eParams = {};
                        eParams[eType] = fn;
                        eParams.scope = me.controller;
                        
                        var comp = body.down(selector.replace(/^window\s+/, ''));
                        if (comp) {
                            comp.on(eParams);
                        }
                    });
                }
            });
        }
        
        return body;
    },
    
    
    openEditWindow: function(record) {
        if (this.windowCfg.editable || this.windowCfg.showInfo) {
            return Ext.create('Ext.window.Window', Ext.Object.merge({                        
                title: "ჩანაწერის დეტალები",
                modal: true,
                autoScroll: false,
                autoShow: true,
                minWidth: 680,
                maxHeight: 600,
                maxWidth: 1024,
                layout: {
                    type: 'fit'
                },
                items: [
                   this._generateWindowBody(record)
                ],
                listeners: {
                    resize: {
                        fn: '_onWindowResize',
                        scope: this.controller
                    }
                }
            }, this.windowCfg));
        } else {
            return null;
        }
    }
    
});