Ext.define('Mux.XViewController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.xview',
    
    requires: [
        'Mux.window.Confirm'
    ],
    
    control: {
        '#': {
            render: '_onGridRender',
            celldblclick: '_onItemDoubleClick'
        },
        
        'filters': {
            submit: '_onFiltersSubmit'
        },
        
        "button#add": {
            click: '_onItemAddClick'
        },
        
        "actioncolumn": {
            itemedit: '_onItemEditClick',
            itemdelete: '_onItemDeleteClick'
        },
        
        '#download-excel': {
            click: '_onDownloadExcelClick'
        }
    },
    
    
    _onWindowResize: function(window) {
        window.center();
    },
    
    
    _onDownloadExcelClick: function() {
        var baseUrl = this.getView().store.getProxy().api.read;
        var url = Ext.String.format('{0}{1}&action=data&excel=1', window.location.pathname, baseUrl);
        var params = this.getView().down('filters').getValues();
        var win = window.open(url + '&' + Mux.util.Util.objToQueryStr(params), '_blank');
        win.focus();
    },
            
            
    _onTabChange: function(tabPanel) {
        tabPanel.up('window').center();
    },
    
    
    _onGridRender: function(grid) {
        var store = grid.getStore();
        var filterBar = grid.down('filters');
        
        store.on('beforeload', function(store, op) {
            if (filterBar) {
                store.getProxy().extraParams = Ext.Object.merge(store.extraParams || {}, filterBar.getValues());
            }
        });
        
        store.on('load', function() {
            if (filterBar) {
                store.getProxy().extraParams = store.extraParams || {};
            }
        });
        
        if (!store.getRemoteSort()) {
            store.setRemoteSort(true);
        }
        
        store.load();
    },
    
    
    _onFiltersSubmit: function() {
        var store = this.getView().getStore();
        store.currentPage = 1;
        store.load();
    },
    
    
    _onItemDoubleClick: function(grid, td, cellIndex, record, tr, rowIndex) {
        if (!this.getView().disableDoubleClickEdit) {
            this._onItemEditClick(grid, grid.getStore().getAt(rowIndex));
        }
    },
    
    
    _onItemAddClick: function() {
        this.getView().openEditWindow();
    },
    
    
    _onItemEditClick: function(grid, record) {
        this.getView().openEditWindow(record);
    },
            
            
    _onItemDeleteClick: function(grid, record) {
        var me = this;
        
        Ext.create('Mux.window.Confirm', {
            title: 'გაფრთხილება',
            msg: 'ნამდვილად გსურთ მოცემული ჩანაწერის წაშლა?',
            icon: Ext.Msg.QUESTION,
            timestamp: this.getView().deleteTimestamp,
            fn: function(buttonId, timestamp) {
                if (buttonId === 'yes') {
                    me.removeRecord(record, timestamp);
                    this.close();
                }
            }
        });
    },
    
    
    _onEditCancelClick: function(button, e, options) {
        button.up('window').close();
    },
            
    
    _onEditSubmitClick: function(button) {
        var me = this;
        var window = button.up('window');
        var form = window.down('form');
        
        if (!form.getForm().isValid()) {
            return false;
        }
        
        var grid = this.getView();
        var store = grid.getStore();
        var record = form.record;
        var params = store.model.nameToMapping(form.getValues());

        window.setLoading(true);

        Ext.Ajax.request({
            url: record ? store.getProxy().api.update : store.getProxy().api.create,
            params: params,
            success: function(conn) {
                var result = Ext.decode(conn.responseText);
                if (result.success) {
                    if (record) {
                        Mux.util.Util.notify('ინფორმაცია წარმატებით შეიცვალა!', '');
                    } else {
                        Mux.util.Util.notify('ინფორმაცია წარმატებით დაიმატა!', '');
                    }
                                        
                    if (me.fireViewEvent('itemsaved', Ext.Object.merge({}, params, { id: result.id }))) {
                        window.close();
                        
                        // if we need to re-open the edit dialog for newly added items
                        if (!record && result.id && grid.reopenAfterInsert) {
                            // try to acquire the item locally
                            store.readOne(result.id, function(err, record) {
                                if (!err) {
                                    grid.openEditWindow(record);
                                } else {
                                    Mux.util.Util.showErrorMsg(err);
                                }
                            });
                        }
                        
                        store.load();
                    } else {
                        window.setLoading(false);
                    }
                } else {
                    Mux.util.Util.showErrorMsg(result.msg);
                    window.setLoading(false);
                }
            },
            failure: function(conn) {
               Mux.util.Util.showErrorMsg(conn.responseText || "სერვერთან დაკავშირება ვერ მოხერხდა"); 
               window.setLoading(false);
            }
        });
    },
    
    
    removeRecord: function(record, timestamp) {
        var grid = this.getView();
        var store = grid.getStore();
        
        Ext.Ajax.request({
            url: store.getProxy().api.destroy,
            params: {
                id: record.get('id'),
                end_date: timestamp || Ext.Date.format(new Date, 'Y-m-d H:i:s')
            },
            success: function(conn) {
                var result = Ext.decode(conn.responseText);
                if (result.success) {
                    Mux.util.Util.notify('ჩანაწერი წარმატებით წაიშალა!', '');
                    store.load();
                } else {
                    Mux.util.Util.showErrorMsg(result.msg);
                }
            },
            failure: function(conn) {
               Mux.util.Util.showErrorMsg(conn.responseText || "სერვერთან დაკავშირება ვერ მოხერხდა"); 
            }
        });
    }
        

});
