Ext.define('Mux.data.FilterStore', {
    extend: 'Ext.data.Store',
    
    autoLoad: false,
    lastOptions: {},
    
    extraParams: {},
    
    constructor: function() {
        this.callParent(arguments);
        
        var url = 'load.php?a={0}.{1}.grid';
        var classParts = this.self.getName().split('.').slice(-2);       
        
        this.setProxy({
            type: 'ajax',
            url: this.proxyUrl || Ext.String.format(url, classParts[0], classParts[1].replace(/^Filter/, '')),

            pageParam: false,
            startParam: false,
            limitParam: false,
            
            extraParams: this.extraParams,

            reader: {
                root: 'topics',
                totalProperty: 'totalCount'
            },

            writer: {
                nameProperty: 'mapping'
            }
        });        
    }
    
});
