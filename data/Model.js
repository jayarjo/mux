Ext.define('Mux.data.Model', {
    extend: 'Ext.data.Model',
    
    inheritableStatics: {
        getMapping: function() {            
            return this.nameToMapping.apply(this, arguments);
        },
                
        nameToMapping: function(name) {
            var me = this;
            var mappedFields = {};
            if (Ext.isObject(name)) {
                Ext.iterate(name, function(key, value) {
                    mappedFields[me.nameToMapping(key)] = value;
                });
                return mappedFields;
            }
            
            var field = Ext.Array.filter(this.getFields(), function(field) {
                return field.name === name;
            });
            return field.length ? field[0].mapping || name : name; 
        },
                
        mappingToName: function(mapping) {
            var me = this;
            var mappedFields = {};
            if (Ext.isObject(mapping)) {
                Ext.iterate(mapping, function(key, value) {
                    mappedFields[me.mappingToName(key)] = value;
                });
                return mappedFields;
            }
            
            var field = Ext.Array.filter(this.getFields(), function(field) {
                return field.mapping === mapping;
            });
            
            return field.length ? field[0].name : mapping; 
        }
    }
    
});
