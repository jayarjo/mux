Ext.define('Mux.data.writer.FormData', {
    extend: 'Ext.data.writer.Writer',
    alternateClassName: 'Soft.data.FormDataWriter',
    alias: 'writer.formdata',

    writeRecords: function(request, data) {
        if (!request.params) {
            request.params = {};
        }
        
        if (data.length === 1) {
            Ext.Object.merge(request.params, data[0]);
        } else {
            Ext.each(data, function(item, i) {
               Ext.iterate(item, function(key, value) {
                   request.params[Ext.String.format('{0}[{1}]', key, i)] = value;
               }); 
            });
        }
        return request;
    }
});
