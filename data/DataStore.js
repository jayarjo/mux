Ext.define('Mux.data.DataStore', {
    extend: 'Ext.data.Store',
   
    requires: [
        'Mux.data.proxy.PseudoRest',
        'Mux.data.writer.FormData'
    ],
    
    autoSync: true,
    autoLoad: false,
    lastOptions: {},
    
    sorters: [{
        property: 'id',
        direction: 'ASC'
    }],

    remoteSort: false,
    
    pageSize: 35,
    
    urlPattern: 'load.php?a={1}.{2}.{0}',
    proxyApi: null,
    
    extraParams: {},
        
    constructor: function(config) {        
        var api = {};
        var apiMap = {
            create: 'addEdit',
            read: 'grid',
            readOne: 'byId',
            update: 'addEdit',
            destroy: 'destroy',
            info: 'info'
        };
        
        var classParts = this.self.getName().split('.').slice(-2);
        var urlPattern = config.urlPattern || this.urlPattern;
        
        // substitute module and controller names
        Ext.iterate(apiMap, function(action, method) {
            api[action] = Ext.String.format(urlPattern, method, classParts[0], classParts[1]);
        }, this);
        
        config.proxy = {
            type: 'pseudo_rest',
            api: config.proxyApi || api,
            extraParams: config.extraParams,
            reader: {
                rootProperty: 'topics',
                totalProperty: 'totalCount'
            },
            writer: {
                type: 'formdata',
                //writeAllFields: false,
                nameProperty: 'mapping'
            }
        };
        
        this.callParent([config]);
    },
            
    
    readOne: function(id, callback) {
        var me = this;
        var classParts = this.self.getName().split('.').slice(-2);
        var url = this.proxyApi && this.proxyApi.readOne || Ext.String.format(this.urlPattern, 'byId', classParts[0], classParts[1]);
        
        callback = callback || Ext.emptyFn;
        
        Ext.Ajax.request({
            url: url,
            params: {
                id: id
            },
            success: function(conn, response, options, eOpts) {
                var data = Ext.decode(conn.responseText);
                callback.call(me, null, Ext.create(me.model, me.model.mappingToName(data)));                                   
            },
            failure: function(conn, response, options, eOpts) {
                callback.call(me, conn.responseText);
            }
        });
    }
    
});
