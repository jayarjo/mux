Ext.define('Mux.data.proxy.PseudoRest', {
    extend: 'Ext.data.proxy.Ajax',
    
    alias : 'proxy.pseudo_rest',

    actionMethods: {
        create : 'POST',
        read   : 'GET',
        update : 'POST',
        destroy: 'POST'
    },

    appendId: true,

    
    batchActions: false,


    buildUrl: function(request) {
        var me        = this,
            operation = request.getOperation(),
            records   = operation.records || [],
            record    = records[0],
            format    = me.format,
            url       = me.getUrl(request),
            id        = record ? record.getId() : operation.id;
    
        if (me.appendId && Ext.isNumeric(id)) {
            url = Ext.String.urlAppend(url, Ext.String.format("id={0}", id));
        }

        if (format) {
            if (!url.match(/\.$/)) {
                url += '.';
            }

            url += format;
        }

        request.url = url;

        return me.callParent(arguments);
    }
});
