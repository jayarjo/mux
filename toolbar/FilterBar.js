Ext.define('Mux.toolbar.FilterBar', {
    extend: 'Ext.form.Panel',
    
    alias: 'widget.filters',
    
    border: false,
    
    searchField: true,
    searchEnabled: true,
    searchPlaceholder: "ფრაზა",
    
    bodyPadding: '3 5 3 5',
    bodyStyle: {
        backgroundColor: 'transparent'
    },
    
    cls: 'x-toolbar-default',

    layout: {
        type: 'hbox',
        align: 'left'
    },

    defaults: {
        margin: "0 2 0 2",
        hideLabel: true,
        labelAlign: 'right',
        labelWidth: 100,
        labelStyle: {
            'overflow': 'hidden',
            'text-overflow': 'ellipsis'
        },
        matchFieldWidth: false,
        listConfig: {
            maxWidth: 200
        } 
    },
    
    defaultType: 'combobox',
    
 
    initComponent: function() {
        
       // customize items
        var items = [];
        
        var labelDefaults = {
            xtype: 'label',
            margin: '3 3 0 5'
        };
        
        var comboDefaults = {
            forceSelection: true,
            anyMatch: true,
            listConfig: {
                maxWidth: 450,
                minWidth: 150
            }
        };
        
        if (this.items && this.items.length) {
            Ext.each(this.items, function(item) {
                var text = item.fieldLabel || item.label;
                
                if (item.xtype === 'button') {
                    item.ui = 'default-toolbar';
                }
                
                if (text) {
                    var label = Ext.apply({
                        text: text
                    }, labelDefaults);

                    Ext.apply(item, {
                        hideLabel: true
                    });
                    items.push(label);
                }
                
                if (!item.xtype || Ext.Array.indexOf(['combobox', 'dependobox'], item.xtype) !== -1) {                    
                    item.xtype = 'dependobox'; // dirty, but effective                    
                    
                    if (item.dependsOn) {
                        item = Ext.apply({
                            xtype: 'dependobox',
                            disabled: true,
                            queryMode: 'local'
                        }, item);
                    }
                    Ext.Object.merge(item, comboDefaults);
                }    
                
                items.push(item);
            }, this);
        }
        
        items.push({ xtype: 'tbfill' });
        
        if (this.searchField) {
            items.push({
                xtype: 'textfield',
                name: 'sword',
                width: 100,
                itemId: 'sword',
                emptyText: this.searchPlaceholder,
                listeners: {
                    specialkey: function(comp, e) {
                        if (e.getKey() === e.ENTER) {
                            comp.up('form').fireEvent('submit', comp.up('form').getForm().getValues());
                        }
                    }
                }
            });
        }
        
        if (this.searchEnabled) {
            [].push.apply(items, [
                {
                    xtype: 'button',
                    ui: 'default-toolbar',
                    tooltip: 'ველების გასუფთავება',
                    itemId: 'clear',
                    iconCls: 'icon-filter-clear',
                    listeners: {
                        click: function(comp) {                            
                            Ext.each(comp.up('form').query('combobox, textfield, hiddenfield, numberfield'), function(comp) {                            
                                if (Ext.Array.indexOf(comp.xtype, ['combobox', 'dependobox']) && comp.dependsOn) {
                                    comp.store.loadData([], false);
                                    comp.store.lastQuery = null;
                                    comp.reset();
                                    comp.disable();
                                }
                                
                                comp.reset();
                            });
                            
                            // do not reload the grid automatically on clear
                            // comp.up('form').fireEvent('submit', comp.up('form').getForm().getValues());
                        }
                    }
                },
                {
                    xtype: 'button',
                    ui: 'default-toolbar',
                    text: 'ძებნა',
                    itemId: 'search',
                    listeners: {
                        click: function(comp) {
                            comp.up('form').fireEvent('submit', comp.up('form').getForm().getValues());
                        }
                    }
                }
            ]);
        }
        
        this.items = items;
                
        this.callParent(arguments);
    },
    
    
    getValues: function() {
        var store = this.up('grid').getStore();
        var fields = this.getForm().getFields();
        var mappedFields = {};
        
        fields.each(function(field, idx, length) {
            var mapping;
            var name = field.getName();
            var value = ['datefield', 'monthfield'].indexOf(field.xtype) !== -1 ? field.getSubmitValue() : field.getValue();
            
            if (field.storeMapping) {
                mappedFields[field.storeMapping] = value;
            } else if (store.model.getMapping && (mapping = store.model.getMapping(name))) {
                mappedFields[mapping] = value;
            } else {
                mappedFields[name] = value;
            }
        }, this);
        return mappedFields;
    }
    
    
});
