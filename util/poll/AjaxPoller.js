Ext.define('Mux.util.poll.AjaxPoller', {
    extend: 'Mux.util.poll.Poller',
    
    config: {
        url: null,
        interval: 3000,
        lastResponse: null
    },
    
    _task: null,
    
    _message: {},
    
    _defaultParams: {},
    
    
    _ping: function() {
        var me = this;
        
        if (me._state === Mux.util.poll.Poller.SENDING) {
            return;
        }
        me._state = Mux.util.poll.Poller.SENDING;
                
        Ext.Ajax.request({
            url: this.getUrl(),
            params: Ext.apply({}, this._message, this._defaultParams),
            callback : function(options, success, response) {
                me._message = {};
                
                if (success) {
                    if (response.responseText !== me.getLastResponse()) {
                        me.fireEvent('message', Ext.decode(response.responseText, true));
                    }
                }
                me.setLastResponse(response.responseText);
                me._state = Mux.util.poll.Poller.OPENED;
               
            }
        });
    },
    
    open: function(params) {
        var me = this;
        
        if (!me.callParent(arguments)) {
            return false;
        }
        
        me._defaultParams = params || {};
        
        me._task = {
            run: me._ping,
            scope: me,
            interval: me.getInterval()
        };
                
        Ext.TaskManager.start(me._task);
        
        me._state = Mux.util.poll.Poller.OPENED;
        me.fireEvent('open');
    },
            
            
    send: function(message) {
        this._message = message;
    },
            
    
    close: function() {
        this.callParent(arguments);
        
        this._task.destroy();
        this._message = this._defaultParams = {};
        this.fireEvent('close');
    }
});
