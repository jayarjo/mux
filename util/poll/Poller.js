Ext.define('Mux.util.poll.Poller', {
    
    requires: [
        'Ext.Ajax',
        'Ext.TaskManager'
    ],
    
    mixins: {
        observable: 'Ext.util.Observable'
    },
    
    statics: {
        CLOSED: 0,
        OPENING: 1, 
        OPENED: 2,
        SENDING: 4,
        RECEIVING: 5
    },
    
    task: null,
    
    config: {
        url: null
    },
    
    _state: 0,
    
    constructor: function(config) {
        this.initConfig(config);
        this.mixins.observable.constructor.call(this);
        
        this.addEvents(
            'open',
            'close',
            'message',
            'error'
        );
            
        return this;
    },
            
    open: function() {
        if (!this.getUrl()) {
            this.fireEvent('error', "url property cannot be empty.");
            return false;
        }
        
        if (this._state > 0) {
            this.fireEvent('error', "poller already opened.");
            return false;
        }
        
        this._state = Mux.util.poll.Poller.OPENING;
        return true;
    },
    
    send: Ext.emptyFn,
    
    close: function() {
        this._state = Mux.util.poll.Poller.OPENING;
    }
});

