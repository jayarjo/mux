Ext.define('Mux.util.SessionMonitor', {
    
    config: {
        interval: 1000 * 10,  // run every 10 seconds.
        maxInactive: 1000 * 60 * 15,  // 15 minutes of inactivity allowed; set it to 1 for testing.
        logoutCallback: Ext.emptyFn,
        refreshCallback: Ext.emptyFn,
        ui: Ext.getBody()
    },
    
    lastActive: null,
    sessionTask: null,
        
    constructor: function(config) {
        this.initConfig(config);
           
        // session monitor task
        this.sessionTask = {
            run: this.monitorUI,
            interval: this.interval,
            scope: this
        };
    },
            
    
    _onActivity: function() {
        this.lastActive = new Date();
    },
            
            
    /**
    * Starts the session timer task and registers mouse/keyboard activity event monitors.
    */
    monitorStart : function() {
        this.lastActive = new Date();

        this.getUi().on('mousemove', this._onActivity, this);
        this.getUi().on('keydown', this._onActivity, this);

        Ext.TaskManager.start(this.sessionTask);
    },
            
 
    /**
     * Stops the session timer task and unregisters the mouse/keyboard activity event monitors.
     */
    monitorStop: function() {
          Ext.TaskManager.stop(this.sessionTask);

          this.getUi().un('mousemove', this._onActivity, this);  //  always wipe-up after yourself...
          this.getUi().un('keydown', this._onActivity, this);
    },
     
            
    monitorUI : function() {
        var now = new Date();
        var inactive = (now - this.lastActive);

        if (inactive >= this.getMaxInactive()) {
            this.monitorStop();

            this.showTimeoutWarning();
            Ext.TaskManager.start(this.countDownTask);
        }
    },
            
            
    showTimeoutWarning: function() {
        var me = this;
        var win = Ext.QueryComponent.query('sessiontimeoutwindow');
        if (win.length) {
            return;
        }
        
        Ext.create('Ext.window.Window', {
            alias: 'widget.sessiontimeoutwindow',
            title: 'გაფრთხილება სესიის ამოწურვის თაობაზე',
            bodyPadding: 5,
            width: 325,
            closable: false,
            modal: true,
            resizable: false,
            
            countDownTask: null,
            remaining: 60,
                        
            listeners: {
                render: function() {
                    // session timeout task, displays a 60 second countdown
                    // message alerting user that their session is about to expire.
                    this.countDownTask = {
                        run: this.countDown,
                        interval: 1000,
                        scope: this
                    };
                },
                        
                destroy: function() {
                    Ext.TaskManager.stop(this.countDownTask);
                }
            },
            
            items: [
                {
                    xtype: 'container',
                    frame: true,
                    html: "თქვენი მხრიდან არ დაფიქსირდა არანაირი აქტივობა 15 წუთის განმავლობაში.<br /><br />"
                }, {
                  xtype: 'label',
                  text: ''
                }
            ],
            
            buttons: [
                {
                    text: 'სესიის გახანგრძლივება',
                    handler: function(button) {
                        Ext.TaskManager.stop(this.countDownTask);
                        button.up('window').close();
                        me.start();
                        me.getRefreshCallback();
                    }
                }, {
                    text: 'სისტემიდან გასვლა',
                    action: 'logout',
                    handler: function(button) {
                        Ext.TaskManager.stop(this.countDownTask);
                        button.up('window').close();
                        me.getLogoutCallback();
                    }
                }
            ],
            
            countDown: function() {
                this.down('label').update('თქვენი სესია ამოიწურება ' +  this.remaining + ' წამში');
    
                if (--this.remaining < 0) {
                    me.fireEvent('timeout');
                }
            }
        });
    },
    
            
    destroy: function() {
        var win = Ext.QueryComponent.query('sessiontimeoutwindow');
        if (win.length) {
            win[0].close();
        }
        
        this.monitorStop();
    }
    
});
