Ext.define('Mux.util.Util', {
    
    statics : {

        required: '<span style="color:red;font-weight:bold; float:right" data-qtip="Required">*</span>',

        decodeJSON : function (text) {

            var result = Ext.JSON.decode(text, true);

            if (!result){
                result = {};
                result.success = false;
                result.msg = text;
            }

            return result;
        },
                
        notify : function (title, format) {            
            function createBox (t, s) {
                return '<div class="msg"><h3>' + t + '</h3><p>' + s + '</p></div>';
            }
            
            var msgDiv = Ext.get('msg-div');
            if (!msgDiv) {
                msgDiv = Ext.DomHelper.insertFirst(document.body, {id:'msg-div'}, true);
            }

            var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
            var m = Ext.DomHelper.append(msgDiv.dom, createBox(title, s), true);
            m.hide();
            m.slideIn('t').ghost("t", { delay: 3000, remove: true});
        },

        showErrorMsg: function (text) {

            Ext.Msg.show({
                title:'შეცდომა!',
                msg: text,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
        },
                
        
        objToQueryStr: function(obj) {
            return Object.keys(obj).reduce(function(a,k) {
                if (obj[k]) {
                    a.push(k+'='+encodeURIComponent(obj[k]));
                }
                return a; 
            },[]).join('&');
        },
        
        
        toDate : function (text) {
            var date = text.replace(/-/g, '/');
            return new Date(date);
        },
                
        
        isEmpty : function(v, allowBlank) {
            if (typeof v === 'object' || typeof v === 'function') {
                var i;
                for (i in v) {
                    if (v.hasOwnProperty(i)) {
                        return false;
                    }
                }
                return true;
            } else {
                // not an object or function - same as before
                return v === null || v === undefined || (!allowBlank ? v === '' : false);
            }
        },
                
        /**
         * Transforms set of interdependent rows into a hierarchical tree ready
         * to be wrapped into a TreeStore.
         * 
         * @method array2Tree
         * @static
         * @param {Array} data Flat array of tree items
         * @param {Object} options
         *  @param {Boolean} [options.checkboxTree=true] Whether it should be a tree with checkboxes
         *  @param {String} [options.idProperty='id'] Property to be used as items id
         *  @param {String} [options.parentIdProperty='parent_id'] Property to be used as items parent id
         *  @param {String} [options.textProperty='id'] Property to be used as items title
         *  @param {String} [options.checkeddProperty='perm'] Property to be used as identifier of items state (checked or not)
         * @return {Array} Multi-dimensional array in format of the TreeStore
         */
        array2Tree: function(data, options) {
            var sortedData = {};
            var storeData = [];
            var orphanData = {};
            
    
            options = Ext.Object.merge({
                checkboxTree: true,
                textProperty: 'description',
                idProperty: 'id',
                parentIdProperty: 'parent_id',
                checkedProperty: 'perm',
                itemDefaults: {
                    leaf: true
                },
                parentItemDefaults: {
                    leaf: false,
                    cls: 'folder'
                }
            }, options);
            

            Ext.each(data, function(row) {
                var id = row[options.idProperty];
                
                var item = Ext.apply({
                    id: id,
                    text: Ext.String.trim(row[options.textProperty])
                }, options.itemDefaults);
                
                if (options.checkboxTree) {
                    item.checked = !!row[options.checkedProperty];
                }
                
                if (!sortedData.hasOwnProperty(id)) {
                    sortedData[id] = item;
                    
                    var parentId = row[options.parentIdProperty];
                    if (!parentId || parentId == 0) { // has no parent
                        Ext.apply(item, options.parentItemDefaults);
                        storeData.push(item);
                    } else {
                        if (!sortedData[parentId]) {
                            if (!orphanData[parentId]) {
                                orphanData[parentId] = [];
                            }
                            orphanData[parentId].push(item);
                        } else {
                            if (!sortedData[parentId].children) {
                                Ext.apply(sortedData[parentId], {
                                    children: []
                                }, options.parentItemDefaults);
                            }
                            sortedData[parentId].children.push(item);
                        }
                    }      
                }                                 
            });
            
            // now put back any items that still do not have a parent
            Ext.iterate(orphanData, function(parentId, items) {
                if (sortedData.hasOwnProperty(parentId)) {
                    [].push.apply(sortedData[parentId].children, items);
                    delete orphanData[parentId];
                }
            });
            
            // unset folder state from items that do not have children
            Ext.each(storeData, function(item) {
                if (!item.children || !item.children.length) {
                    item.leaf = true;
                }
            });
            
            return storeData;
        },
                
        
        secsPassed: function(startDate, endDate) {
            function leftPad(val) {
                return Ext.String.leftPad(val, 2, '0');
            }

            var secsLeft = Mux.util.Util.diff(startDate, endDate, Ext.Date.SECOND);
            var mins = Math.floor(secsLeft / 60);
            if (mins >= 60) {
                mins %= 60;
            }
            var secs = secsLeft % 60;
            return [leftPad(mins), leftPad(secs)].join(':');
        },
                
        
        diff: function (min, max, unit) {
            var ExtDate = Ext.Date, est, diff = +max - min;
            switch (unit) {
                case ExtDate.MILLI:
                    return diff;
                case ExtDate.SECOND:
                    return Math.floor(diff / 1000);
                case ExtDate.MINUTE:
                    return Math.floor(diff / 60000);
                case ExtDate.HOUR:
                    return Math.floor(diff / 3600000);
                case ExtDate.DAY:
                    return Math.floor(diff / 86400000);
                case 'w':
                    return Math.floor(diff / 604800000);
                case ExtDate.MONTH:
                    est = (max.getFullYear() * 12 + max.getMonth()) - (min.getFullYear() * 12 + min.getMonth());
                    if (Ext.Date.add(min, unit, est) > max) {
                        return est - 1;
                    } else {
                        return est;
                    }
                case ExtDate.YEAR:
                    est = max.getFullYear() - min.getFullYear();
                    if (Ext.Date.add(min, unit, est) > max) {
                        return est - 1;
                    } else {
                        return est;
                    }
            }
        }

        
    }
});