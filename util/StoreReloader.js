Ext.define('Mux.util.StoreReloader', {
    
    mixins: {
        observable: 'Ext.util.Observable'
    },
    
    constructor: function() {
        this.mixins.observable.constructor.call(this);
        
        this.addEvents(
            'load'            
        );
    },
            
    load: function(stores) {
        var self = this, loadStates = {};
        
        function checkStoreStates() {
            var done = true;
            Ext.iterate(loadStates, function(storeId, state) {
                if (!state) {
                    done = false;
                    return false;
                }
            });
            return done;
        }
        
        Ext.iterate(stores, function(storeId, params) {
            if (Ext.getStore(storeId)) {
                loadStates[storeId] = false;
            }
        });
        
        Ext.iterate(stores, function(storeId, params) {
            var store = Ext.getStore(storeId);
            if (!store) {
                return true;
            }

            store.on('load', function onLoadStore() {
                store.un('load', onLoadStore);
                loadStates[store.storeId] = true;
                if (checkStoreStates()) {
                    self.fireEvent('load');
                    self = null;
                }
            });

            store.load({
                params: params
            });
        }, this);        
    }
});
