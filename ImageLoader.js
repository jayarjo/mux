Ext.define('Mux.ImageLoader', {
	requires: [
		'Ext.util.HashMap'
	],
	
	singleton: true,

	LOADED: 2,
	ERROR: 1,
	ABORTED: 3,
	LOADING: 4,

	config: {
		slots: 1
	},

	_completed: {},
	_queued: [],
	_loading: new Ext.util.HashMap(),


	constructor: function(config) {
		this.initConfig(config);
		return this;
	},


	add: function(src, callback) {
		this._queued.push({
			src: src,
			cb: callback || Ext.emptyFn
		});

		this._queueNext();
	},

	clear: function() {
		var me = this;
		this._queued.forEach(function(item) {
			item.cb.call(me, me.ABORTED);
		});
		this._queued = [];
	},


	onAllDone: Ext.emptyFn,


	_queueNext: function() {
		if (this._loading.getCount() >= this.getSlots()) {
			return;
		}

		var me = this;

		var item = this._queued.pop();
		if (item) {
			var img = new Image();
			img.onload = function() {
				me._onImageDone(true, this);
			};
			img.onerror = function() {
				me._onImageDone(false, this);
			};

			me._loading.add(item.src, item.cb);
			item.cb.call(me, me.LOADING);
			img.src = item.src;
		}
	},


	_onImageDone: function(success, img) {
		var me = this;
		var src = img.src;
		var cb = this._loading.get(src);
		this._loading.removeAtKey(src);

		this._completed[src] = success;

		if (success) {
			cb.call(me, me.LOADED, img);
		} else {
			cb.call(me, me.ERROR);
		}

		setTimeout(function() {
			me._queueNext.call(me);
		}, 100);
	}

});
